package explorer;

import java.io.IOException;

public class Main {
  private static String url = "/wiki/Ubuntu";

  public static void main(String[] args) {

    Explorer explorer = new MultiThreadedExplorer();
    try {
      explorer.explore(url);
    } catch(Exception e) {
      System.out.println(e);
    }
  }
}
