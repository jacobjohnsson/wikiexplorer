package explorer;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.io.IOException;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.jsoup.Jsoup;

public class LinkProducer implements Runnable {

  private static final int WAIT_TIME_MS = 5000;

  private String from;
  private BlockingQueue<Link> workQueue;
  private String urlExtension;
  private ExecutorService pool;
  private Document doc;

  public LinkProducer (String from, BlockingQueue<Link> workQueue, String url, ExecutorService pool) {
    this.from = from;
    this.workQueue = workQueue;
    this.urlExtension = url;
    this.pool = pool;
  }

  public void run() {

    try {
      doc = Jsoup.connect("https://en.wikipedia.org" + urlExtension).get();
    } catch(Exception e) {
      System.out.println("URL Extension fails: " + urlExtension);
      System.out.println(e);
    }

    workQueue.add(new Link(from, doc));

    try {
      Thread.sleep(WAIT_TIME_MS);
    } catch(Exception e) {
      System.out.println("LinkProducer interruped!");
    }


    Elements links = doc.select("a[href^=/wiki]");
    for (Element link : links) {

      // String debugLink = link.attr("href");
      //System.out.println("\tadded " + debugLink);

      LinkProducer finder = new LinkProducer(doc.title(), workQueue, link.attr("href"), pool);
      pool.submit(finder);
    }
  }

}
