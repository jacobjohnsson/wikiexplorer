package explorer;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class MultiThreadedExplorer implements Explorer {

  private final int upperBound = 10;
  private final ExecutorService producers = Executors.newFixedThreadPool(2);
  private final int N_CONSUMERS = 2;

  public void explore (String url) throws IOException {

    BlockingQueue<Link> queue = new LinkedBlockingQueue(upperBound);

    producers.submit(new LinkProducer("Ground Zero", queue, url, producers));

    for (int i = 0; i < N_CONSUMERS; i++) {
      new Thread(new TitlePrintingConsumer(queue)).start();
    }
  }
}
