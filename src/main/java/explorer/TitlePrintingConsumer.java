package explorer;

import java.util.concurrent.BlockingQueue;

import org.jsoup.nodes.Document;

public class TitlePrintingConsumer implements Runnable {

  BlockingQueue<Link> queue;

  public TitlePrintingConsumer  (BlockingQueue<Link> queue) {
    this.queue = queue;
  }

  public void run() {
    String title = "Unknown Title";
    while (true) {
      try {
        Link link = queue.take();
        System.out.println(link.getFrom() + ", " + link.getTo().title());
      } catch(Exception e) {
        System.out.println("Consumer Interrupted!");
      }
    }
  }

}
