package explorer;

import java.io.IOException;

public interface Explorer {

  public void explore (String url) throws IOException;
  
}
