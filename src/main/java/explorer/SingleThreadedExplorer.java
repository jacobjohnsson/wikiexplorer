package explorer;

import java.io.IOException;
import java.util.List;
import java.util.LinkedList;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.jsoup.Jsoup;

public class SingleThreadedExplorer implements Explorer {
  private Document doc;

  public void explore (String url) throws IOException {
    List<String> queue = new LinkedList<String>();
    queue.add(url);

    while (!queue.isEmpty()) {
      String urlExtension = "";
      try {
        urlExtension = queue.remove(0);
        doc = Jsoup.connect("https://en.wikipedia.org" + urlExtension).get();
      } catch(Exception e) {
        System.out.println("URL Extension fails: " + urlExtension);
        System.out.println(e);
        throw new IOException();
      }

      System.out.println(doc.title());

      Elements links = doc.select("a[href^=/wiki]");
      for (Element link : links) {
        String stringLink = link.attr("href");
        // System.out.println(stringLink);
        queue.add(stringLink);
      }
    }
    System.out.println("Done!");
  }
}
