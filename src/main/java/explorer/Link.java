package explorer;

import org.jsoup.nodes.Document;

// IMMUTABLE
public class Link {

  private String from;
  private Document to;

  public Link  (String from, Document to) {
    this.from = from;
    this.to = to;
  }

  public String getFrom() {
    return from;
  }

  public Document getTo() {
    return to;
  }
}
